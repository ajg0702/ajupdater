plugins {
    java
}

group = "us.ajg0702"
version = "1.0.0"

repositories {
    mavenCentral()
    maven { url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots/") }
}

dependencies {
    testImplementation("junit", "junit", "4.12")
    compileOnly("org.spigotmc:spigot-api:1.16.2-R0.1-SNAPSHOT")
}

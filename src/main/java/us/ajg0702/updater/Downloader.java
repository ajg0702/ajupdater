package us.ajg0702.updater;

import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Downloader {
    public static String download(String pluginName, String pluginspath, String currentVersion,  String version) {
        try {
            URL url = new URL("https://ajg0702.us/pl/updater/jars/"+pluginName+"-"+version+".jar");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.addRequestProperty("User-Agent", pluginName+"/"+currentVersion);

            ReadableByteChannel rbc = Channels.newChannel(con.getInputStream());
            FileOutputStream fos = new FileOutputStream(pluginspath+pluginName+"-"+version+".jar");
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            return "good";

        } catch(Exception e) {
            return "error: "+e.getMessage();
        }
    }
}

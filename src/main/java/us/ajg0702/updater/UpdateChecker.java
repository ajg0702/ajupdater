package us.ajg0702.updater;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UpdateChecker {

    protected static String getLatestVersion(String pluginName, String currentVersion) {
        try {
            URL url = new URL("https://ajg0702.us/pl/updater/get.php?plugin="+pluginName);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.addRequestProperty("User-Agent", pluginName+"/"+currentVersion);
            InputStream inputStream = connection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            StringBuilder string = new StringBuilder();
            while ((line = br.readLine()) != null) {
                string.append(line);
            }

            String latestVersion = string.toString();

            return latestVersion;

        } catch (Exception e) {
            return "offline: "+e.getMessage();
        }

    }
}

package us.ajg0702.updater;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.regex.Matcher;

public class Updater {
    private static Updater instance;
    public static Updater getInstance() {
        return instance;
    }
    public static Updater getInstance(JavaPlugin plugin) {
        if(instance == null) {
            instance = new Updater(plugin);
        }
        return instance;
    }

    private JavaPlugin plugin;
    private String pluginName;
    private String currentVersion;
    private String latestVersion;


    private Updater(JavaPlugin plugin) {
        this.plugin = plugin;
        pluginName = plugin.getName();

        currentVersion = plugin.getDescription().getVersion().split("-")[0];
    }

    private boolean enabled = true;
    public boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(boolean en) {
        enabled = en;
    }


    /**
     * Schedule the check timer (and unschedule the previous one if it exists)
     */
    private int updateInterval = -1;
    public void scheduleChecks() {
        unscheduleChecks();
        updateInterval = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this::check, 40, 20*60*60).getTaskId();
    }

    /**
     * Unschedule the check timer
     */
    public void unscheduleChecks() {
        if(updateInterval != -1) {
            try {
                Bukkit.getScheduler().cancelTask(updateInterval);
            } catch(Exception ignored) {}
        }
    }

    private boolean updateAvailable = false;

    /**
     * Check if there is an update available.
     */
    public void check() {
        if(!enabled) return;
        latestVersion = UpdateChecker.getLatestVersion(pluginName, currentVersion);
        if(latestVersion.startsWith("offline: ")) {
            plugin.getLogger().warning("Unable to check for update! Reason: "+latestVersion.substring("offline:".length()));
        }
        updateAvailable = VersionUtils.versionGreater(currentVersion, latestVersion);
    }

    /**
     * Check if there is an update available
     * @return If there is an update available
     */
    public boolean isUpdateAvailable() {
        return updateAvailable;
    }

    /**
     * Downloads the latest jar and replaces the old jar.
     * @return "good" if downloaded successfullt. "nocurjar" if the jar for the current version cannot be found. "error: ..." if an error occurred during the actual download
     */
    public String downloadLatest() {
        String curjarname = pluginName+"-"+currentVersion+".jar";
        String[] slashparts = pl.getDataFolder().toString().split(Matcher.quoteReplacement(File.separator));

        StringBuilder pluginspath = new StringBuilder();
        int i = 0;
        for(String part : slashparts) {
            pluginspath.append(part).append(File.separator);
            i++;
            if(i+1 >= slashparts.length) break;
        }

        File oldjar = new File(pluginspath+curjarname);
        if(!oldjar.exists()) {
            oldjar = new File(pluginspath+pluginName+".jar");
            if(!oldjar.exists()) {
                return "nocurjar";
            }
        }

        String output = Downloader.download(pluginName, pluginspath.toString(), currentVersion, latestVersion);
        if(output.equals("good")) {
            oldjar.delete();
        }
        return output;
    }
}

package us.ajg0702.updater;

public class VersionUtils {
    public static boolean versionGreater(String v1, String v2) {
        String[] v1parts = v1.split("\\.");
        String[] v2parts = v2.split("\\.");

        int i = 0;
        for(String v1p : v1parts) {
            String v2p;
            if(v2parts.length < v1parts.length && i > v2parts.length-1) {
                v2p = "0";
            } else {
                v2p = v2parts[i];
            }

            int v1prev = 0;
            int v2prev = 0;
            if(i != 0) {
                v1prev = Integer.parseInt(v1parts[i-1]);
                v2prev = Integer.parseInt(v2parts[i-1]);
            }

            int v1n = Integer.parseInt(v1p);
            int v2n = Integer.parseInt(v2p);

            if(v2n > v1n && v1prev <= v2prev) {
                return true;
            }

            i++;
        }
        return false;
    }
}

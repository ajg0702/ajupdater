import org.junit.Test;
import us.ajg0702.updater.VersionUtils;

public class VersionUtilsTest {

    @Test
    public void versionGreaterTest() throws Exception {
        if(VersionUtils.versionGreater("1.0.0", "1.0.0")) {
            throw new Exception("1.0.0 should NOT be greater than 1.0.0");
        }
        int tested = 1;
        for(int i1 = 1; i1 <= 10; i1++) {
            for(int i2 = 0; i2 <= 10; i2++) {
                for(int i3 = 0; i3 <= 10; i3++) {
                    if(i1 == 1 && i2 == 0 && i3 == 0) continue;

                    tested++;

                    String nv = i1+"."+i2+"."+i3;
                    if(!VersionUtils.versionGreater("1.0.0", nv)) {
                        throw new Exception(nv+" should be greater than 1.0.0");
                    }
                    if(VersionUtils.versionGreater(nv, "1.0.0")) {
                        throw new Exception("1.0.0 should NOT be greater than "+nv);
                    }
                }
            }
        }
        System.out.println("Tested "+tested+" versions");
    }
}